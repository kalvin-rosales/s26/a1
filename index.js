//1 What directive is used by Node.js in loading the modules it needs?
//require()
//2 What Node.js module contains a method for server creation?
//createServer()
//3 What is the method of the http object responsible for creating a server using Node.js?
//CreateServer(requestListener())
//4 What method of the response object allows us to set status codes and content types?
//response.write()
//5 Where will console.log() output its contents when run in Node.js?
//console terminal
//6 What property of the request object contains the address' endpoint?
//response.end()





const HTTP = require('http');

HTTP.createServer((request, response)=> {
    
    if(request.url === "/login"){
        response.writeHead(200, {"Content-Type":"text/plain"});
        response.write("Welcome to the login page.");
        response.end()
    
    } 
    else if (request.url === "/register"){
        response.writeHead(404, {"Content-Type":"text/plain"});
        response.write("I'm sorry the page you are looking for cannot be found.");
        response.end()
    } else {
        response.writeHead(500, {"Content-Type":"text/plain"});
        response.write("Error");
        response.end()
    }

}).listen(3000)